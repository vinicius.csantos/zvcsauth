package br.com.vinicius.santos.auth;

import br.com.vinicius.santos.auth.Config.Config;
import br.com.vinicius.santos.auth.Database.MySqlConnector;
import br.com.vinicius.santos.auth.Database.Scripts.CreateDatabases;
import org.bukkit.plugin.java.JavaPlugin;

public class AuthPlugin extends JavaPlugin {

    @Override
    public void onEnable() {

        new Config(this);
        new MySqlConnector(this);
        new CreateDatabases();

        super.onEnable();
    }

    @Override
    public void onDisable() {

        final MySqlConnector mySqlConnector = new MySqlConnector();
        mySqlConnector.closeConnection();

        super.onDisable();
    }
}
