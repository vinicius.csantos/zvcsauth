package br.com.vinicius.santos.auth.Config;

import br.com.vinicius.santos.auth.AuthPlugin;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {

    public Config(AuthPlugin authPlugin){

        FileConfiguration fileConfiguration = authPlugin.getConfig();

        //DATABASE INFO
        fileConfiguration.addDefault("database.host", "localhost");
        fileConfiguration.addDefault("database.port", 3306);
        fileConfiguration.addDefault("database.database", "dbName");
        fileConfiguration.addDefault("database.username", "username");
        fileConfiguration.addDefault("database.password", "password");

        //MESSAGES
        fileConfiguration.addDefault("message.prefix", "&f[&6zvcsAuth&f] ");
        fileConfiguration.addDefault("message.register", "&cPlease, use </register [password] [password]> to register");
        fileConfiguration.addDefault("message.login", "&cPlease, use </login [password]> to sign in");


        fileConfiguration.options().copyDefaults(true);

        authPlugin.saveConfig();

    }

}
