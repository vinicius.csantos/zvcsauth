package br.com.vinicius.santos.auth.Database.Scripts;

import br.com.vinicius.santos.auth.Database.MySqlConnector;

public class CreateDatabases {

    public CreateDatabases() {
        this.createUsers();
    }

    public void createUsers() {

        String sql = "CREATE TABLE IF NOT EXISTS auth_users (\n";
        sql += "id BIGINT auto_increment NOT NULL,\n";
        sql += "uuid varchar(36) NOT NULL UNIQUE,\n";
        sql += "username varchar(16) NOT NULL UNIQUE,\n";
        sql += "password varchar(100) NOT NULL,\n";
        sql += "logged BOOL DEFAULT false NOT NULL,\n";
        sql += "user_ip varchar(100) NOT NULL,\n";
        sql += "last_location varchar(100) NULL,\n";
        sql += "CONSTRAINT users_PK PRIMARY KEY (id)\n";
        sql += ")\n";
        sql += "ENGINE=InnoDB\n";
        sql += "DEFAULT CHARSET=utf8\n";
        sql += "COLLATE=utf8_unicode_ci;";

        MySqlConnector mySqlConnector = new MySqlConnector();
        mySqlConnector.executeUpdate(sql);

    }

}
