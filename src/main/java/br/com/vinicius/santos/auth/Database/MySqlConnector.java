package br.com.vinicius.santos.auth.Database;

import br.com.vinicius.santos.auth.AuthPlugin;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.*;

public class MySqlConnector {
    static Connection connection;

    public MySqlConnector(AuthPlugin authPlugin) {
        FileConfiguration fileConfiguration = authPlugin.getConfig();
        final String username = fileConfiguration.getString("database.username");
        final String password = fileConfiguration.getString("database.password");
        final String database = fileConfiguration.getString("database.database");
        final String host = fileConfiguration.getString("database.host");
        final int port = fileConfiguration.getInt("database.port");
        final String url = String.format("jdbc:mysql://%s:%s/%s", host, port, database);

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public MySqlConnector() {
    }

    ;

    public Connection getConnection() {
        return connection;
    }

    public int executeUpdate(String sql) {
        int result = 0;
        try {

            if (connection.isValid(10000)) {
                PreparedStatement statement = connection.prepareStatement(sql);

                result = statement.executeUpdate();

                return result;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ResultSet executeQuery(String sql) {
        try {
            if (connection.isValid(10000)) {
                PreparedStatement statement = connection.prepareStatement(sql);

                return statement.executeQuery(sql);
            }

            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void closeConnection() {

        try {
            if (connection != null && !connection.isClosed()) {

                connection.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
